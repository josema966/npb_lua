require "nanoplayboard"


board.communication.connect(self,"/dev/ttyUSB0","_57600");

board.rgb.on(self);
print("on")
board.arduino.delay(500)
board.rgb.toggle(self)
print("toggle")
board.arduino.delay(500)
-- aqui para comentar
board.rgb.toggle(self)
print("toggle")
board.arduino.delay(500)
board.rgb.off(self)
print("off")
board.arduino.delay(1000)
print ("set intensity")
board.rgb.on(self)
for intensity = 0,255 do
  board.rgb.setIntensity(intensity)
  board.arduino.delay(20)
end

print("Buzzer")
board.buzzer.playTone(self,800,1000)
board.arduino.delay(500)
board.buzzer.stopTone(self)
print("Rojo")
board.rgb.setColor(self,255,0,0)
board.arduino.delay(1000)
print("Verde")
board.rgb.setColor(self,0,255,0)
board.arduino.delay(1000)
print("Azul")
board.rgb.setColor(self,0,0,255)
board.arduino.delay(1000)
board.rgb.off(self)
print("off")
board.arduino.delay(1000)

-- Prueba de servos para un robot que lleva servos enfrentados
--
--[[
board.arduino.delay(1000)

board.servo.to(self,0,90)
board.servo.to(self,1,90)
print("90 - 90.- ajustar servos para que esten parados")
board.arduino.delay(1000)


board.servo.to(self,0,180)
board.servo.to(self,1,0)
print("0 - 180.- adelante o atrás")
board.arduino.delay(1000)

board.servo.to(self,0,0)
board.servo.to(self,1,1800)
print("0 - 180.- al reves del anterior")
board.arduino.delay(1000)

board.servo.to(self,0,90)
board.servo.to(self,1,90)
print("90 - 90.- De nuevo parado")

print("Caracteres")
board.ledMatrix:printChar(self,"o")
board.arduino.delay(500)
board.ledMatrix:printChar(self,"k")
board.arduino.delay(500)
board.ledMatrix:printChar(self," ")
board.arduino.delay(500)
print("numeros")
board.ledMatrix:printNumber(self,50)
board.arduino.delay(500)
board.ledMatrix:printNumber(self,64)
board.arduino.delay(500)
board.ledMatrix:stopPrint(self)
board.arduino.delay(500)
board.ledMatrix.printString(self,"Hola a todos")
board.arduino.delay(6000)
board.ledMatrix:stopPrint(self)


for ax = 0, 100 do
  print (ax)
  na = board.potentiometer.read(self)
  print("na.- "..na)
  board.arduino.delay(500)
end
-- Fin del comentario

for ax = 0, 100 do
  print (ax)
  na = board.ldr.read(self)
  print("na.- "..na)
  board.arduino.delay(500)
end
--]]