--[[ 
NanoPlayBoard constants.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU  General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.
--]]
--[[
 a = {}     -- create a table and store its reference in `a' --]]
 
print "entra en nano_constants"

NanoConstants = {
	EXTENDED_ID				 = 0X00;
--[[ Modos de operacion que nos puede devolver]]
	DIGITAL_INPUT      		 = 0x00;
	DIGITAL_OUTPUT     		 = 0x01;
	ANALOG_INPUT   		     = 0x02;
	PWM                		 = 0x03;
	SERVO              		 = 0x04;
	SHIFT              		 = 0x05;
	I2C                		 = 0x06;
	ONEWIRE            		 = 0x07;
	STEPPER            		 = 0x08;
	ENCODER            		 = 0x09;
	SERIAL             		 = 0x0A;
	INPUT_PULLUP       		 = 0x0B;
	
--[[Comandos reservados para la NanoPlayBoard]]
    COMMAND                  = 0x10;--- 16
    BUZZER_PLAY_TONE         = 0x20;--- 32
    BUZZER_STOP_TONE         = 0x21;
    RGB_ON                   = 0x22;--- 34
    RGB_OFF                  = 0x23;--- 35
    RGB_TOGGLE               = 0x24;
    RGB_SET_COLOR            = 0x25;--- 37
    RGB_SET_INTENSITY        = 0x26;--- 38

    POTENTIOMETER_READ       = 0x27;
    POTENTIOMETER_SCALE_TO   = 0x28;

    LDR_READ                 = 0x29;
    LDR_SCALE_TO             = 0x30;

    LEDMATRIX_PRINT_CHAR     = 0x31;
    LEDMATRIX_PRINT_PATTERN  = 0x32;
    LEDMATRIX_PRINT_STRING   = 0x33;
    LEDMATRIX_PRINT_NUMBER   = 0x34;
    LEDMATRIX_STOP_PRINT     = 0x35;

    SERVO_TO                 = 0x36;
    SERVOS_GO_FORWARD        = 0x37;
    SERVOS_GO_BACKWARD       = 0x38;
    SERVOS_GO_RIGHT          = 0x39;
    SERVOS_GO_LEFT           = 0x40; --- 64
    SERVOS_SET_SPEED         = 0x41;

    ROTARY_ENCODER_READ      = 0x42;

    ULTRASOUND_READ          = 0x43;
    ULTRASOUND_SCALE_TO      = 0x44;

    DHT_READ_TEMPERATURE     = 0x45;
    DHT_READ_HUMIDITY        = 0x46;

    BUTTON_TOP_IS_PRESSED    = 0x47;
    BUTTON_DOWN_IS_PRESSED   = 0x48;
    BUTTON_LEFT_IS_PRESSED   = 0x49;
    BUTTON_RIGHT_IS_PRESSED  = 0x50;

    ACCELEROMETER_GET_X      = 0x51;
    ACCELEROMETER_GET_Y      = 0x52;
	
--[[ Fin de los comandos reservados para la NanoPlayBoard
		Comienzan los comandos del protocolo los añado todos aunque no se usen
		Comienzan los de Sysex.
	--]]
	--                         0x60; -- 96
	ANALOG_MAPPING_QUERY     = 0x69;
	ANALOG_MAPPING_RESPONSE  = 0x6A;
	CAPABILITY_QUERY         = 0x6B;
	CAPABILITY_RESPONSE      = 0x6C;
	PIN_STATE_QUERY          = 0x6D;
	PIN_STATE_RESPONSE       = 0x6E;
	EXTENDED_ANALOG          = 0x6F;
	STRING_DATA              = 0x71;
	REPORT_FIRMWARE          = 0x79;
	SAMPLING_INTERVAL        = 0x7A;
	SYSEX_NON_REALTIME       = 0x7E;
	SYSEX_REALTIME           = 0X7F;
	
--[[ Comandos principales --]]	
	DIGITAL_MESSAGE 		 = 0x90;
    ANALOG_MESSAGE 			   = 0xE0;
    REPORT_ANALOG 			   = 0xC0;
    REPORT_DIGITAL 			   = 0xD0;
    SET_PIN_MODE 		    	 = 0xF4;
	SET_DIGITAL_PIN_VALUE	   = 0xF5;
    REPORT_VERSION		  	 = 0xF9;
    SYSTEM_RESET		    	 = 0xFF;
    START_SYSEX				     = 0xF0; --240
    END_SYSEX				       = 0xF7 -- 247
	}
