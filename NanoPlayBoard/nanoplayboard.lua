--[[ 
NanoPlayBoard class.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU  General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.
--]]

print("Entra en nanoplayboard.lua")
require "nanoplaycore"
require "socket"

board={}

board.communication = {}

  function board.communication:connect(puer,baud)
    local msg
  --[[  print (puer)
    print (baud)
    print ("por puerto")
    --]]
    if puer == nil then puer = '/dev/ttyUSB0' end
    if baud == nil then baud = '_57600' end
    
    pp = NanoPlayCore.connect(self,puer,baud)
    
    while msg == nil or  msg<'1' do
      msg = pp:read(64,500)
    end
    na = string.sub(msg,8,8)
    pl = string.sub(msg,16,16)
    bo = string.sub(msg,24,24)
    if  na=="N" and pl=="P" and bo=="B" then
      print ("NanoPlayBoard detectada")
    else
      print("NanoPlayBoard no detectada")
    end
  end

  board.rgb ={}
    function board.rgb:setColor(r, g, b)
      local command =	NanoConstants.COMMAND
      local data = NanoConstants.RGB_SET_COLOR
      --[[
      print("rojo",r)
      print("verde",g)
      print("azul",b)
      --]]
      local p1 = r >> 1
      local p2 = ((r%2)<<6 | (g>>2) )
      local p3 = ((g << 5) & 0x7F) | b>>3
      local p4 = (b<<4) & 0x70
      --[[
      print ("p1", p1)
      print ("p2",p2)
      print (g)
      print (g << 5)
      print ("p3",p3)
      print ("p4",p4)
      --]]
      
      NanoPlayCore.send_sysex(self,command, data,p1,p2,p3,p4)	
      
	
    end
    function board.rgb:on()
	    local command =	NanoConstants.COMMAND
      local data = NanoConstants.RGB_ON
	    NanoPlayCore.send_sysex(self,command, data)	
    end
    function board.rgb.off()
      local command =	NanoConstants.COMMAND
      local data = NanoConstants.RGB_OFF
	    NanoPlayCore.send_sysex(self,command, data)	
	
    end
    function board.rgb.toggle()
      local command =	NanoConstants.COMMAND
      local data = NanoConstants.RGB_TOGGLE
	    NanoPlayCore.send_sysex(self,command, data)	
	
    end
    function board.rgb.setIntensity(intensity)
      print ("entro en intensidad "..intensity)
  
      local command =	NanoConstants.COMMAND
      local data = NanoConstants.RGB_SET_INTENSITY
      --local p1 = intensity%128
      --local p2 = intensity>>7 --- AÑADIDO SIN COMPROBAR
      -- p2 = 0
      local p1
      local p2
      p1,p2 = NanoPlayCore.split(this,intensity)
      -- assert(aa==p1,"comprueba primer parametro ")
      --print ("split aa "..aa)
      --print (p1)
      --print ("split bb "..bb)
      --print (p2)
      -- assert(bb==p2,"comprueba segundo parametro ")
      
      print ("Comprobar que funciona p2!!!")
	    NanoPlayCore.send_sysex(self,command, data,p1,p2)
    end

board.buzzer ={}
	function board.buzzer:playTone(frequency_hz, duration_ms)
    local command =	NanoConstants.COMMAND
    local data = NanoConstants.BUZZER_PLAY_TONE
    --[[
    print ("frecuencia", frequency_hz)
    print ("duracion", duration_ms)
    --]]
    local f1 = frequency_hz%128
    local f2 = math.modf(frequency_hz/128)
    local d1 = duration_ms%128
    local d2 = math.modf(duration_ms%128)
    NanoPlayCore.send_sysex(self,command, data,f1,f2,d1,d2)	
    	
	end	
	function board.buzzer:stopTone()
    local command =	NanoConstants.COMMAND
    local data = NanoConstants.BUZZER_STOP_TONE
    NanoPlayCore.send_sysex(self,command, data)	
    
	
	end
	
board.potentiometer ={}

	function board.potentiometer:read()
    local msg = nil
    local d1 = 0
    local d2 = 0
    local d3 = 0
    local d4 = 0
    print "Entro en potentiometer"
    print (msg)
    local command =	NanoConstants.COMMAND
    local data = NanoConstants.POTENTIOMETER_READ
    
    NanoPlayCore.send_sysex(self,command, data)	
    -- print "enviados los datos"
    msg=NanoPlayCore.read_sysex()
    --[[ print("tipo " .. type(msg))
    print (msg)
    print (string.byte(string.sub(msg,1,1)))
    --]]
    if string.byte(string.sub(msg,1,1))== NanoConstants.POTENTIOMETER_READ then
       d1 = string.byte(string.sub(msg,3,3))
       d2 = string.byte(string.sub(msg,4,4))
       d3 = string.byte(string.sub(msg,5,5))
       d4 = string.byte(string.sub(msg,6,6))
      --[[
      print ("d1", d1)
      print ("d2",d2)
      print ("d3",d3)
      print ("d4",d4)
      --]]
       d3 = d3<<8
       d2 = d2<<7
       d1 = d1+d2+d3
       -- print ("d1 "..d1)
       return d1
    else
      print "No respondió a la orden"
    end
    
    -- print "Finalizo potentiometer"
	
end	

	function board.potentiometer:scale_to(to_low, to_high, callback)
	
	end

board.ldr ={}

	function board.ldr:read(callback)
    local msg = nil
    local d1 = 0
    local d2 = 0
    local d3 = 0
    local d4 = 0
    print "Entro en LDR"
    print (msg)
    local command =	NanoConstants.COMMAND
    local data = NanoConstants.LDR_READ
    
    NanoPlayCore.send_sysex(self,command, data)	
    -- print "enviados los datos"
    msg=NanoPlayCore.read_sysex()
    --[[ print("tipo " .. type(msg))
    print (msg)
    print (string.byte(string.sub(msg,1,1)))
    --]]
    if string.byte(string.sub(msg,1,1))== NanoConstants.LDR_READ then
       d1 = string.byte(string.sub(msg,3,3))
       d2 = string.byte(string.sub(msg,4,4))
       d3 = string.byte(string.sub(msg,5,5))
       d4 = string.byte(string.sub(msg,6,6))
      --[[
      print ("d1", d1)
      print ("d2",d2)
      print ("d3",d3)
      print ("d4",d4)
      --]]
       d3 = d3<<8
       d2 = d2<<7
       d1 = d1+d2+d3
       -- print ("d1 "..d1)
       return d1
    else
      print "No respondió a la orden"
    end
    
    -- print "Finalizo LDR"
	
	end	
	function board.ldr:scale_to(to_low, to_high, callback)
	
	end
	
board.ledMatrix ={}	

function board.ledMatrix:printChar(self,caracter)
  local command =	NanoConstants.COMMAND
  local data = NanoConstants.LEDMATRIX_PRINT_CHAR
  local p1 = string.byte(caracter) & 0x7F
  NanoPlayCore.send_sysex(self,command, data,p1)	
  
end
function board.ledMatrix:printNumber(self,number)
  local command =	NanoConstants.COMMAND
  local data = NanoConstants.LEDMATRIX_PRINT_CHAR
  local p1 = number & 0x7F
  NanoPlayCore.send_sysex(self,command, data,p1)	
  
end
function board.ledMatrix:printString(mensa)
  --print(mensa)
  --print(type(mensa))
  local p1={}
  local lon = string.len(mensa)
  local command =	NanoConstants.COMMAND
  local data = NanoConstants.LEDMATRIX_PRINT_STRING
  -- table.insert(p1,(self))
  table.insert(p1,(command))
  table.insert(p1,(data))
  table.insert(p1,(lon & 0x7F))
    
  for a=1 , lon do
    
    d = string.byte(mensa,a)
    print(a,d)
    table.insert(p1,(d & 0x7F))
    print (p1[a])
  end
  -- print ("Realizada la cosa")
  NanoPlayCore.send_sysex(self,p1)	
  
end
function board.ledMatrix:stopPrint(self)
  local command =	NanoConstants.COMMAND
  local data = NanoConstants.LEDMATRIX_STOP_PRINT
   NanoPlayCore.send_sysex(self,command, data)	
  
end

board.servo ={}

function board.servo:to(servo,grados)
  local command =	NanoConstants.COMMAND
  local data = NanoConstants.SERVO_TO
  servo = servo%2
  local d1 = (grados & 0x7F)
  local d2 = (grados >>7)
  print ("d1",d1)
  print ("d2",d2)
  print ("grados",grados)
  
  NanoPlayCore.send_sysex(self,command, data,servo,d1,d2)	
end


--[[
RotayEncoder ={}

	function RotaryEncoder.read(callback)
	
end	


Ultrasound ={}
	function Ultrasound.read(callback)
	
	end	

	function NanoPlayBoard.sleep(self, interval)
	
	end


print "Hola mundo"
print(COMMANDO)
print(COMMAND)
print(NanoConstants.COMMAND)
print(NanoConstants.BUZZER_STOP_TONE)
print(NanoConstants.ACCELEROMETER_GET_Y)
--]]
board.arduino={}

function board.arduino.delay(millis)
  --- hace lo que tenga que hacer
 
  local start= socket.gettime()*1000
  repeat until socket.gettime()*1000>start+millis 
 
end
